# BURU : Antoine Ghesquiere et Benjamin Feldman

## Fonctionnalités
*IMPORTANT : tout est en Python 2.7 et en Django 1.11*

Appli web en Django :
* scan de morceaux en ligne
* compte utilisateur
* historique propre à chaque utilisateur
* classement des morceaux les plus scannés

## How-to-use :

Pour ajouter un morceau à la base de donner, il faut :
* l'ajouter depuis l'interface administration de Django (remplir les champs appropriés, et uploader le fichier .mp3 ; le champ sha1 n'a pas d'importance, on peut y mettre n'importe quoi, il sera rempli automatiquement par la suite):
* répéter cette étape tant qu'on souhaite ajouter des morceaux
* exécuter la commande suivante, dans le répertoire shazam_python_ag_bf/buru/ :
```
$ python manage.py shell
> from scan.views import sha1_every_song
> sha1_every_song()
```
* retourner à la racine du projet, et exécuter create_database.py pour créer les fingerprints des morceaux que l'on vient d'ajouter :
```
$ python buru/scan/reconnaissance/create_database.py
```

Voilà !

Ci-dessous un petit compte-rendu de la semaine.

## Jour 1

Pratiquement toute la journée a été consacrée à l'installation (avec succès !) de dejavu. Finalement nous utiliserons Python 2.7...
L'utilisation de MySQL sur OS X a posé un gros souci.
Il a fallu :
* passer en 2.7
* diverses mise à jour de pip, pyaudio...
* modifier le module DejaVu (changer un - en xor)

Le système de reconnaissance de fichiers et par le micro a été développé par Benjamin avec DejaVu.

L'interface a commencé à être développée par Antoine.

## Jour 2

Interface Tkinter terminée et fonctionnelle. Lors d'un scan, l'appli propose une liste de morceaux du même artiste.

##### Interface web avec Django, MVP :

* mêmes fonctionnalités que l'interface Tkinter
* compte utilisateur avec historique des recherches
* classement des chansons les plus recherchées

##### User story

Utilisateur enregistré : peut utiliser le service et avoir accès à son historique de recherches

Visiteur : a quand même accès au service, sans historique.

## Jour 3

Django, encore.

## Jour 4

L'appli Django est finie et fonctionnelle (néanmoins le micro écouté est celui du serveur...).

