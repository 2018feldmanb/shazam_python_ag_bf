#-*-coding:utf-8
"""buru URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
import scan.views

app_name = "main"

urlpatterns = [
    url(r'admin/', admin.site.urls),
    url(r'^$', scan.views.index, name='index'), #permet d'executer views.index() lorsqu'on accède à localhost:8000/
    url(r'scan/', include('scan.urls')),
    url(r'^(?P<song_id>[0-9]+)/$', scan.views.detail, name="detail"),
    url(r'^result/', scan.views.scan_mic, name="result"),
    url(r'^accounts/', include('accounts.urls')),
    url('accounts/', include('django.contrib.auth.urls')),
    url(r'^history/', scan.views.history, name='history')
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [

        # For django versions before 2.0:
        url(r'^__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns
