# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Artist, Song

admin.site.register(Artist)
admin.site.register(Song)

# Register your models here.
