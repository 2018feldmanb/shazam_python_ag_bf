# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Artist(models.Model):
  artist_name = models.CharField(max_length=250)
  def __str__(self):
    return self.artist_name


class Song(models.Model):

  song_title = models.CharField(max_length=250)
  song_artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
  song_plays = models.IntegerField(default=0) #nombre d'écoutes
  song_sha1 = models.CharField(max_length=250, default='')
  file = models.FileField(upload_to='scan/reconnaissance/mp3', default='None')

  def __str__(self):
    return self.song_title



class User(models.Model):
  username = models.CharField(max_length=250)
  password = models.CharField(max_length=250) #en clair pour l'instant.....
  def __str__(self):
    return self.username

class History(models.Model): #historique de TOUS les scan
  history_song = models.IntegerField(default=0)
  history_user = models.IntegerField(default=0)
  date = models.DateField(default="2000-01-01")
