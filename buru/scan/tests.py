from django.urls import resolve
from django.test import TestCase
from scan.views import index
from django.http import HttpResponse, HttpRequest

# Create your tests here.


class WelcomePageTest(TestCase):

    def test_root_url_resolves_to_welcome_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_welcome_page_returns_correct_html(self):
        request = HttpRequest()
        response = index(request)
        html = response.content.decode('utf8')
        message = "Welcome to Buru."
        self.assertTrue(html==message)
    def test_history_page_resolves(self):
        found = resolve('/scan/history')
        self.assertEqual(found.func, index)


