import warnings
import json
warnings.filterwarnings("ignore")

from dejavu import Dejavu
# importe la BDD depuis un fichier JSON
with open("dejavu.cnf") as f:
    config = json.load(f)

if __name__ == '__main__':

	# instance DejaVu
	djv = Dejavu(config)

	# Fingerprint all the mp3's in the directory we give it
	djv.fingerprint_directory("mp3", [".mp3"])
