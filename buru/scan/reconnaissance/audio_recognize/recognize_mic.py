#-*- coding: utf-8

import json
from dejavu import Dejavu
from dejavu.recognize import MicrophoneRecognizer




def recognize(secondes, config_json): # reconnait depuis le micro, pendant "secondes" secondes avec la config sql

    # importe la BDD depuis un fichier JSON.
    config_json = str(config_json)
    with open(config_json) as f:
        config = json.load(f)
    # instance DejaVu

    djv = Dejavu(config)

    song = djv.recognize(MicrophoneRecognizer, secondes)
    if song is None:
        return 0
    else:
        return song


