#-*-coding:utf-8

import Tkinter as tk
from audio_recognize import recognize_file, recognize_mic
import tkFileDialog
from PIL import Image, ImageTk
from database import similar

TIME = 10
root = tk.Tk()
root.geometry("500x300-10+10")

def enterMP3():
    file = tkFileDialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("audio files","*.mp3"),("all files","*.*")))
    song = recognize_file.recognize(file)
    tk.Label(root,text="Le morceau est %s" % song['song_name']).grid(row=3,column=1)
    tk.Label(root,text="Morceaux du même artiste : \n %s" % similar(song['song_name'])).grid(row=4,column=1)
    print song['song_name']
    print similar(song['song_name'])

def enterMicro():
    time = TIME
    song = recognize_mic.recognize(time)
    print song['song_name']
    print similar(song['song_name'])

    if type(song) == str:
        tk.Label(root,text="Morceau inconnu").grid(row=3,column=1)
    else:

        tk.Label(root,text="Le morceau est %s" % song['song_name']).grid(row=3,column=1)
        tk.Label(root,text="Morceaux du même artiste : \n %s" % similar(song['song_name'])).grid(row=4,column=1)
        print(song['confidence'])

def get_value(val):
    TIME = val


im_import = Image.open("icons/import.jpg")
ph_import = ImageTk.PhotoImage(im_import)

button_mp3 = tk.Button(root,image=ph_import,activebackground = "blue",fg="red",command=enterMP3)
button_mp3.grid(row=1,column=1)
root.grid_rowconfigure(0, weight=1)
root.grid_rowconfigure(3, weight=1)
root.grid_columnconfigure(0, weight=1)
root.grid_columnconfigure(3, weight=1)



im_record = Image.open("icons/record.png")
ph_record = ImageTk.PhotoImage(im_record)

button_mic = tk.Button(root,fg="blue",image=ph_record,command=enterMicro)
button_mic.grid(row=1,column=2)

value = tk.IntVar(root)
scale = tk.Scale(root, from_=0, to=10, showvalue=False, variable=value, label="Time", tickinterval=10, orient='h',command=get_value,length=50,sliderlength=10)
scale.grid(row=2, column=2)

root.mainloop()

