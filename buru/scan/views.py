# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import sys
sys.path.insert(0, '../../')
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from models import Song, Artist, History
from django.utils import timezone
import reconnaissance.audio_recognize.recognize_mic as mic
import hashlib
import binascii


def index(request): #page d'accueil, affichage les 5 chansons les plus écoutées
    billboard = Song.objects.order_by('-song_plays')[:5]
    context = {
        'billboard': billboard
    }
    return render(request, 'index.html', context)

def history(request):
    return HttpResponse("zob")

def scan_result(request, song_id): #affiche la chanson lorsqu'elle est scannée
    return HttpResponse("The song you scanned is %s." % song_id)

def detail(request, song_id): #affiche le détail d'une chanson
    song = get_object_or_404(Song, pk=song_id)
    artist = Artist.objects.get(id=song.song_artist_id)
    context = {'song' : song, 'artist' : artist}
    return render(request, 'detail.html', context)

def register(request):
    context = {'username':'bite'}
    return render(request, 'scan/register.html', context)


def scan_mic(request):
    secondes = 5 #arbitraire pour l'instant
    song = mic.recognize(5, '/Users/bjf/PycharmProjects/shazam_python_ag_bf/buru/scan/reconnaissance/audio_recognize/dejavu.cnf') #a changer en repertoire dynamique
    if song == 0:
        context = {'error':True}
        return render(request, 'scan/result.html', context)
    else:
        sha1 = song['file_sha1']
        try:
            song_object = Song.objects.raw('SELECT * FROM scan_song WHERE song_sha1 = \''+sha1+'\';')[0]
            song_object.song_plays += 1
            song_object.save()
            title = song_object.song_title
            if request.user.is_authenticated:
                history_row = History(history_song = song_object.id, history_user = request.user.id, date=timezone.now())
                history_row.save()
            artist = str(Artist.objects.get(pk=song_object.song_artist_id))
        except IndexError: #modifier le template
            artist = "Not"
            title = "Found"
        context = {'artist':artist, 'song':title}
        return render(request, 'scan/result.html', context)


def sha1_every_song():
    songs = Song.objects.raw('SELECT * FROM scan_song')
    ids = [k.id for k in songs]
    for id in ids:
        song = Song.objects.get(pk=id)
        name = unicode(song.file.name)
        file_address = str(name)
        file = open(file_address).read()
        sha1_hash = hashlib.sha1(file).hexdigest()
        song.song_sha1 = sha1_hash
        song.save()

def history(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/')
    sql_request = "SELECT * FROM scan_history WHERE history_user="+str(request.user.id)+";"
    entries = History.objects.raw(sql_request)
    songs_id = [k.history_song for k in entries]
    songs = [Song.objects.get(pk=k) for k in songs_id]
    song_titles = [k.song_title for k in songs]
    artist_ids = [k.song_artist_id for k in songs]
    artists = [str(Artist.objects.get(pk=k).artist_name) for k in artist_ids]
    lines = []
    for k in range(len(song_titles)):
        line = artists[k] + ' - ' + song_titles[k]
        lines.append(line)
    lines.reverse()
    context = {'lines':lines}
    return render(request, 'history.html', context)
