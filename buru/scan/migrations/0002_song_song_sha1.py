# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-11-16 09:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scan', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='song',
            name='song_sha1',
            field=models.BinaryField(default=b'0b0', max_length=20),
        ),
    ]
